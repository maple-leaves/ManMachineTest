# 人机考试

## 介绍
本系统实现人机考试功能，能够为广大用户提供一个平台用来考试，适用人群为K12的学生。学生通过PC端进行考试，选定具体的考试场次，在限定时间内完成试卷，试卷作答情况将在一定时间范围内提交至服务器并进行机器评分。

## 技术栈
* 后端
  * Spring Boot
* 前端
  * vue
  * axios 网络库
  * BootStrap


## 安装教程
```bash
	git clone https://gitee.com/maple-leaves/ManMachineTest.git
	cd ./source/front-end/ManMachineTest/Admin
	npm i
	npm run dev
	cd ./source/front-end/ManMachineTest/Client
	npm i
	npm run dev
	cd ./source/back-end/ManMachineTest
	mvn package
```

## usage
* 使用./source/back-end/ManMachineTest/sql下的 base.sql 初始化数据库
* 修改.source/back-end/ManMachineTest/src/main/resources/application.properties 下的数据库配置
* 然后就可以mvn package后台代码，然后就可以运行target目录下的jar包

## 开发
* 后端是一个标准的 Spring Boot + Maven项目，直接 idea 打开 ./source/back-end/ManMachineTest 目录，导入maven项目即可

## 图
![image](./doc/image/userLogin.png)
![image](./doc/image/userLogin1.png)
![image](./doc/image/userLogin2.png)
![image](./doc/image/userIndex.png)
![image](./doc/image/userEditInfo.png)
![image](./doc/image/userEditPwd.png)
![image](./doc/image/userTestList.png)
![image](./doc/image/userEntranceTest.png)
![image](./doc/image/userTestDetail.png)
![image](./doc/image/adminLogin.png)
![image](./doc/image/adminAddPage.png)
![image](./doc/image/adminAddPage1.png)
![image](./doc/image/adminAddProblem.png)


