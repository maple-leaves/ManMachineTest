import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import index from '@/components/Index'
import AdminLogin from '@/components/AdminLogin'
import AddPage from '@/components/AddPage'
import Test from '@/components/Test'

Vue.use(Router)

export default new Router({
  linkActiveClass: 'active-menu',
  mode: 'history', // 改为history模式
  routes: [
    {
      path: '/',
      redirect: '/adminlogin'
    },
    {
      path: '/adminlogin',
      name: 'AdminLogin',
      component: AdminLogin,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/index',
      name: 'index',
      component: index,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/add-page',
      name: 'AddPage',
      component: AddPage,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/HelloWorld',
      name: 'HelloWorld',
      component: HelloWorld,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/Test',
      name: 'Test',
      component: Test,
      meta: {
        keepAlive: true
      }
    }
  ]
})
