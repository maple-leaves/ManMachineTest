import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import index from '@/components/Index'
import TestList from '@/components/TestList'
import UserIndex from '@/components/UserIndex'
import ExamDetail from '@/components/ExamDetail'
import EntranceExam from '@/components/EntranceExam'
import Login from '@/components/Login'

Vue.use(Router)

export default new Router({
  linkActiveClass: 'active-menu',
  mode: 'history', // 改为history模式
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/index',
      name: 'index',
      component: index,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/user-index',
      name: 'UserIndex',
      component: UserIndex,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/test-list',
      name: 'TestList',
      component: TestList,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/entrance-exam',
      name: 'EntranceExam',
      component: EntranceExam,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/exam-detail',
      name: 'ExamDetail',
      component: ExamDetail,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/HelloWorld',
      name: 'HelloWorld',
      component: HelloWorld,
      meta: {
        keepAlive: true
      }
    }
  ]
})
