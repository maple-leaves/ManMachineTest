// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../node_modules/bootstrap/dist/js/bootstrap.min.js'
import 'font-awesome/css/font-awesome.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import md5 from './plugins/md5'

Vue.use(md5)
Vue.prototype.axios = axios
Vue.use(VueAxios, axios)
Vue.config.productionTip = false
// axios.defaults.baseURL = 'http://localhost:8050'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
