import $ from 'jquery'
// 学生考试页交互
$(function () {
  // radio和checkbox选中后题目大纲样式改变
  $('.exam-detial-container').on('click', 'input[type="radio"],input[type="checkbox"]', function () {
    var _self = $(this)
    var input_name = _self.attr('name').replace(/\_[0-9]+/, '')

    if (_self.is(':checked') || _self.closest('ol').find('label').is(':checked + label')) {
      $('#' + input_name).addClass('done')
      _self.closest('div.question-board').find('.sub-ans').text('提交').addClass('clickable')
    } else {
      $('#' + input_name).removeClass('done')
      _self.closest('div.question-board').find('.sub-ans').text('提交').removeClass('clickable')
    }
    choiceAnswerData(_self.closest('div.question-board'), false)
  })

  // 单题提交按钮交互
  $('.exam-detial-container').on('click', '.sub-ans', function () {
    var _self = $(this)
    if (_self.html() === '提交' && _self.hasClass('clickable')) {
      var isClickable = _self.attr('class').indexOf('clickable')
      var q = _self.closest('div.question-board')
      if (q.find('input[type="text"]').length > 0) {
        r = inputAnswerData(q, true)
      } else {
        r = choiceAnswerData(q, true)
      }
      if (r || isClickable > 0) {
        _self.text('已提交').removeClass('clickable')
      } else {
        _self.text('提交').addClass('clickable')
      }
    }
  })

  // 试卷保存
  $('#save_paper').on('click', function () {
    saveAnswer()
  })
  // 试卷提交
  $('#submit_paper').on('click', function () {
    var _self = $(this)
    var d = _AnswerData()
    if (d.length < questionCount) {
      return unCompleted(questionCount - d.length)
    }
    return new jDialog().show({
      title: '提示',
      width: 650,
      body: '<p style="text-align:center">考试答题提交后将无法修改，是否确认提交？</p>',
      buttons: jDialog.BUTTON_OK_CANCEL
    }, function (result) {
      var _s = this
      if (result === 'ok') {
        submitAnswer()
      } else {
        _s.close()
      }
    })
  })

  // 显示关闭倒计时
  $('.close-count,.show-count').on('click', function () {
    let _aim = $('.hide-time')
    let isShow = _aim.css('display')
    if (isShow === 'none') {
      _aim.show()
    } else {
      _aim.hide()
    }
  })

  var tips = $('#tips')
  var div_top = tips.offset().top
  var wrapper = $('.wrapper')
  var wrapper_top = wrapper.offset().top
  var height = {
    aside_height: tips[0].offsetHeight, // 浮动栏高度
    wrapper_height: wrapper[0].offsetHeight // 浮动栏父级section高度
  }

  var wrapper_top_h = wrapper_top + height.wrapper_height

  $(window).scroll(function () {
    if ($(this).scrollTop() - div_top > 0 && wrapper_top_h - $(this).scrollTop() > height.aside_height) {
      tips.removeAttr('style')
      tips.css({
        position: 'fixed',
        top: 0,
        width: '340px'
      })
    } else if (wrapper_top_h - $(this).scrollTop() < height.aside_height) {
      wrapper.css('position', 'relative')
      tips.removeAttr('style')
      tips.css({
        position: 'absolute',
        right: 0,
        bottom: '40px',
        width: '340px'
      })
    } else {
      tips.removeAttr('style')
    }
  });

  // 倒计时
  (function () {
    var _self = $('.count em'), l = _self.length

    var timeArr = $.map(_self, function (n) { // 获取每个em的内容并返回新数组
      return $(n).text()
    })

    var count_down = setInterval(function () {
      if (timeArr[l - 1] + timeArr[l - 2] + timeArr[l - 3] === 0) {
        clearInterval(count_down) // 清除interval
        autoSubmitDialog()
      }

      if (timeArr[l - 1] > 0) { // 秒计时
        timeArr[l - 1]--
        if (timeArr[l - 1] < 10) {
          timeArr[l - 1] = '0' + timeArr[l - 1]
        }
        $(_self[l - 1]).text(timeArr[l - 1])
      }
      if (timeArr[l - 1] === 0 && timeArr[l - 2] > 0) { // 分计时
        timeArr[l - 1] = 59
        timeArr[l - 2]--
        if (timeArr[l - 2] < 10) {
          timeArr[l - 2] = '0' + timeArr[l - 2]
        }
        $(_self[l - 2]).text(timeArr[l - 2])
      }
      if (timeArr[l - 2] === 0 && timeArr[l - 3] > 0) { // 时计时
        timeArr[l - 2] = 59
        timeArr[l - 3]--
        if (timeArr[l - 3] < 10) {
          timeArr[l - 3] = '0' + timeArr[l - 3]
        }
        $(_self[l - 3]).text(timeArr[l - 3])
      }
    }, 1000)
  })()
})

if (window.no_answer === 'True') {
  $('div.the-ans').each(function () {
    $(this).html('<p>答案已被设置隐藏</p>')
  })
}
