package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;

/**
 * @Description: 考试细详情类，用于存储多表查询后的结果
 * @Param:
 * @Author:liangjian
 * @Date: 2019.7.10
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExamInformation {
	private Long paperId;

	private String title;
	private String startTime;
	private String duration;

	private Integer totalScore;
	private Integer problemNumber;
	private String examRequirement;
	private String scorePublish;
	private String submissionMethod;
	private boolean isCompleted;
	private Integer score;

	public ExamInformation(Long paperId, String title, String startTime, String duration, Integer totalScore, Integer problemNumber,
						   String examRequirement, String scorePublish, String submissionMethod, boolean isCompleted, Integer score) {
		this.paperId = paperId;
		this.title = title;
		this.startTime = startTime;
		this.duration = duration;
		this.totalScore = totalScore;
		this.problemNumber = problemNumber;
		this.examRequirement = examRequirement;
		this.scorePublish = scorePublish;
		this.submissionMethod = submissionMethod;
		this.isCompleted = isCompleted;
		this.score = score;
	}

}
