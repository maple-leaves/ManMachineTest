package com.example.demo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/*
* @Author:吕雨润
* @Date:  2019.07
*/
@ApiModel("系统用户输出实体")
@Slf4j
@Data
@Accessors(chain = true)
public class UserDto {
	@ApiModelProperty("nickname")
	private String nickname;

	@ApiModelProperty("identity")
	private int identity;

	@ApiModelProperty("school")
	private String school;

	@ApiModelProperty("grade")
	private Integer grade;

	@ApiModelProperty("student_id")
	private Long student_id;

	@ApiModelProperty("phone")
	private String phone;
}