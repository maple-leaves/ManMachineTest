package com.example.demo.dto;


public enum ResultCodeEnum {
	/**
	 * 返回代码
	 */
	RESULT_CODE_SUCCESS(200, "成功处理请求"),
	RESULT_CODE_UNAUTHORIZED(401, "无权限"),
	RESULT_CODE_FORBIDDEN(403, "禁止访问"),
	RESULT_CODE_NOT_FOUND(404, "未找到资源"),
	RESULT_CODE_SERVER_ERROR(500, "服务端出错");

	private int status;
	private String message;

	private ResultCodeEnum(int state, String message) {
		this.status = state;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}
}

