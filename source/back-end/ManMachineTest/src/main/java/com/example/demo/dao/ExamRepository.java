package com.example.demo.dao;

import com.example.demo.entity.Exam;
import com.example.demo.entity.ExamInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface ExamRepository extends JpaRepository<Exam, Long> {

	Exam findByPaperId(Long paperId);

	List<Exam> findByGradeAndSchool(Integer grade, String school);

	@Query("select  new com.example.demo.entity.ExamInformation(e.paperId,e.title,e.startTime,e.duration,e.score,e.problemNumber,e.examRequirement," +
			"e.scorePublish,e.submissionMethod,ue.isCompleted,ue.score) " +
			"from Exam e left  join  UserExam ue on e.paperId=ue.paperId where e.grade=:grade and e.school=:school")
	List<ExamInformation> findExamParm(@Param("grade") Integer grade, @Param("school") String school);
}
