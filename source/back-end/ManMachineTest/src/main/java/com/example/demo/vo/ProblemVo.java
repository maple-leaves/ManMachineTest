package com.example.demo.vo;

import com.example.demo.entity.Problem;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

@Data
@Slf4j
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProblemVo {
	private Problem problem;
	private String answer;

	public ProblemVo(Problem problem, String answer) {
		this.problem = problem;
		this.answer = answer;
	}
}
