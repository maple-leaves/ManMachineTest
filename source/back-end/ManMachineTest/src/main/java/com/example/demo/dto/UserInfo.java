package com.example.demo.dto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

@ApiModel("系统用户输出实体")
@Slf4j
@Data
@Accessors(chain = true)
public class UserInfo {
    @ApiModelProperty("unionid")
    private String unionid;

    @ApiModelProperty("grade")
    private String grade;

    @ApiModelProperty("poolId")
    private String poolId;

    @ApiModelProperty("token")
    private String token;

    @ApiModelProperty("hasTags")
    private String[] hasTags;

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getPoolId() {
        return poolId;
    }

    public void setPoolId(String poolId) {
        this.poolId = poolId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String[] getHasTags() {
        return hasTags;
    }

    public void setHasTags(String[] hasTags) {
        this.hasTags = hasTags;
    }
}