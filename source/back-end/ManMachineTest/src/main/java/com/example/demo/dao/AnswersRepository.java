package com.example.demo.dao;

import com.example.demo.entity.Answers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswersRepository extends JpaRepository<Answers, Integer> {
	Answers findByPaperIdAndProblemIdAndNickname(Long paperId, Long problemId, String nickname);

	List<Answers> findByProblemId(Long strProblemId);
}