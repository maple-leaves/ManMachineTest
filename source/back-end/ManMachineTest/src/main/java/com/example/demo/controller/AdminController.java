package com.example.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.dao.ExamRepository;
import com.example.demo.dao.UserRepository;
import com.example.demo.dto.Result;
import com.example.demo.dto.Result2;
import com.example.demo.entity.Exam;
import com.example.demo.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import io.swagger.annotations.Api;

import java.io.*;
import java.util.*;

@Api("管理员接口接口")
@RestController
@RequestMapping("/api/admin")
@Slf4j
public class AdminController {
	@Autowired
	private ExamRepository examRepository;
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserService userService;
	@PostMapping(value = "/addPaper")
	public Result AddPaper(@RequestBody JSONObject jsonObject) {
		Exam exam = new Exam();

		exam.setDuration(jsonObject.getString("duration"));
		System.out.println(exam.getDuration());
		exam.setExamRequirement(jsonObject.getString("examRequirement"));
		System.out.println(exam.getExamRequirement());
		exam.setGrade(jsonObject.getIntValue("grade"));
		System.out.println(exam.getGrade());
		exam.setPaperId(jsonObject.getLongValue("paperId"));
		System.out.println(exam.getPaperId());
		exam.setProblemNumber(jsonObject.getInteger("problemNum"));
		exam.setSchool(jsonObject.getString("school"));
		exam.setScore(jsonObject.getInteger("score"));
		exam.setScorePublish(jsonObject.getString("scorePublish"));
		exam.setStartTime(jsonObject.getString("startTime"));
		exam.setSubmissionMethod((jsonObject.getString("submissionMethod")));
		exam.setTitle(jsonObject.getString("title"));
		System.out.println(exam);
		try {
			examRepository.save(exam);
			return Result.createBySuccess();
		} catch (Exception e) {
			return Result.createByErrorMessage(e.toString());
		}
	}

	@ApiOperation(value = "登录")
	@PostMapping("/login")
	public ResponseEntity<Result> Login(@RequestBody String jsonString){
		int result=userService.adminLogin(jsonString);
		if(result==1){
			return new ResponseEntity(new Result2().ok(), HttpStatus.OK);
		}
		else{
			return new ResponseEntity(new Result2().clientError("wrong user or password"), HttpStatus.UNAUTHORIZED);
		}
	}
}
