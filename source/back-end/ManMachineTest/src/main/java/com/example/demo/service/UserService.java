package com.example.demo.service;

import com.example.demo.dto.UserDto;
import com.example.demo.entity.User;
import com.example.demo.dao.UserRepository;
import com.example.demo.vo.Name_pwdVo;
import com.example.demo.vo.UserVo;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public int login(String jsonString) {
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		Name_pwdVo name_pwdVo = (Name_pwdVo) JSONObject.toBean(jsonObject, Name_pwdVo.class);
		User user = userRepository.findByNickName(name_pwdVo.getNickname());
		if (user == null) {
			return 0;
		}
		if (user.getPwdMD5().equals(name_pwdVo.getPassword())) {
			return 1;
		} else {
			return -1;
		}
	}

	public UserDto showUserInfo(String jsonString) {
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		String jsonNickname = jsonObject.getString("nickname");
		User user = userRepository.findByNickName(jsonNickname);
		UserDto userDto = new UserDto();
		userDto.setNickname(user.getNickName());
		if (user.isIdentity()) {
			userDto.setIdentity(1);
		} else {
			userDto.setIdentity(0);
		}
		userDto.setSchool(user.getSchool());
		userDto.setGrade(user.getGrade());
		userDto.setStudent_id(user.getStudentId());
		userDto.setPhone(user.getPhone());
		return userDto;
	}

	public void updateUserInfo(String jsonString) {
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		UserVo userVo = (UserVo) JSONObject.toBean(jsonObject, UserVo.class);
		User user = userRepository.findByNickName(userVo.getNickname());
		System.out.println(user);
		user.setIdentity(userVo.getIdentity() == 1);
		System.out.println(user.isIdentity());
		user.setSchool(userVo.getSchool());
		user.setStudentId(userVo.getStudent_id());
		user.setGrade(userVo.getGrade());
		user.setPhone(userVo.getPhone());
		userRepository.save(user);
	}

	public boolean updatePwd(String jsonString) {
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		String previousPwd = jsonObject.getString("previousPwd");
		String password = jsonObject.getString("password");
		Name_pwdVo name_pwdVo = (Name_pwdVo) JSONObject.toBean(jsonObject, Name_pwdVo.class);
		User user = userRepository.findByNickName(name_pwdVo.getNickname());
		if (previousPwd.equals(user.getPwdMD5())) {
			user.setPwdMD5(password);
			userRepository.save(user);
			return true;
		} else {
			return false;
		}
	}


	public int adminLogin(String jsonString){
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		Name_pwdVo name_pwdVo = (Name_pwdVo) JSONObject.toBean(jsonObject,Name_pwdVo.class);
		User user = userRepository.findByNickName(name_pwdVo.getNickname());
		if(user==null||!user.getIs_admin()){
			return 0;
		}
		if(user.getPwdMD5().equals(name_pwdVo.getPassword())){
			return 1;
		}
		else{
			return -1;
		}
	}
}