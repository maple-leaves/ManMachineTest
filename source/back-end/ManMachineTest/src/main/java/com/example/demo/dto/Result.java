package com.example.demo.dto;


import com.example.demo.config.FastJsonEnumSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;


@ApiModel("统一返回对象")
@Slf4j
@Data
@Accessors(chain = true)
public class Result<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty("数据")
	private T data;
	@ApiModelProperty("响应码")
	private Integer status;

	private String message;

	/**
	 * 返回默认的状态码和信息
	 */
	private Result(ResultCodeEnum resultCodeEnum) {
		this.status = resultCodeEnum.getStatus();
		this.message = resultCodeEnum.getMessage();
	}

	/**
	 * 接收状态码，返回状态码和信息
	 *
	 * @param status
	 * @param message
	 */
	private Result(Integer status, String message) {
		this.status = status;
		this.message = message;
	}

	/**
	 * 接收数据，返回数据、状态码、信息
	 *
	 * @param data
	 */
	private Result(T data) {
		this.status = ResultCodeEnum.RESULT_CODE_SUCCESS.getStatus();
		this.message = ResultCodeEnum.RESULT_CODE_SUCCESS.getMessage();
		this.data = data;
	}

	/**
	 * 接收信息和数据，返回信息、数据和状态码
	 *
	 * @param message
	 * @param data
	 */
	private Result(String message, T data) {
		this.status = ResultCodeEnum.RESULT_CODE_SUCCESS.getStatus();
		this.message = message;
		this.data = data;
	}

	/**
	 * @return
	 * @JsonIgnore使之不在json序列化结果当中
	 */
	@JsonIgnore
	public boolean isSuccess() {
		return this.status == ResultCodeEnum.RESULT_CODE_SUCCESS.getStatus();
	}

	/**
	 * 返回状态码
	 *
	 * @return
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * 返回信息
	 *
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * 返回数据
	 *
	 * @return
	 */
	public T getData() {
		return data;
	}

	public static <T> Result<T> createByError() {
		return new Result(ResultCodeEnum.RESULT_CODE_SERVER_ERROR);
	}

	public static <T> Result<T> createByError(ResultCodeEnum resultCodeEnum) {
		return new Result<T>(resultCodeEnum);
	}

	public static <T> Result<T> createByErrorMessage(String message) {
		return new Result(ResultCodeEnum.RESULT_CODE_SERVER_ERROR.getStatus(), message);
	}

	public static <T> Result<T> createByErrorCodeMessage(Integer code, String message) {
		return new Result(code, message);
	}

	public static <T> Result<T> createByResultCodeEnum(ResultCodeEnum resultCodeEnum) {
		return new Result<T>(resultCodeEnum.getStatus(), resultCodeEnum.getMessage());
	}

	public static <T> Result<T> createBySuccess() {
		return new Result(ResultCodeEnum.RESULT_CODE_SUCCESS);
	}

	public static <T> Result<T> createBySuccessMessage(String message) {
		return new Result(ResultCodeEnum.RESULT_CODE_SUCCESS.getStatus(), message);
	}

	public static <T> Result<T> createBySuccess(T data) {
		return new Result(data);
	}

	public static <T> Result<T> createBySuccess(String message, T data) {
		return new Result(message, data);
	}

    /*-------------------------------------------------------------------------------------------------------------*/

}

