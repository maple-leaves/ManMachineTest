package com.example.demo.dao;


import com.example.demo.entity.UserExam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserExamRepository extends JpaRepository<UserExam, Integer> {
	UserExam findByPaperId(Long paperId);
}
