package com.example.demo.dto;

import com.example.demo.vo.UserVo;
import com.example.demo.config.FastJsonEnumSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @Auther: 吕雨润
 * @Date: 2019.07
 */
@ApiModel("统一返回对象")
@Slf4j
@Data
@Accessors(chain = true)
public class Result2<T> implements Serializable {

	@JsonSerialize(using = FastJsonEnumSerializer.class)
	@ApiModelProperty("响应码")
	private HttpStatus status;

	@ApiModelProperty("描述")
	private String message;

	@ApiModelProperty("数据")
	private T data;

	@ApiModelProperty("是否成功")
	private boolean success;

	public Result2() {
	}

	public Result2(HttpStatus status, String message, T data, boolean success) {
		this.status = status;
		this.message = message;
		this.data = data;
		this.success = success;
	}

	public Result2 result(HttpStatus status, String message, T data) {
		return new Result2(status, message, data, true);
	}

	public Result2 ok() {
		return new Result2(HttpStatus.OK, null, null, true);
	}

	public Result2 ok(String message) {
		return new Result2(HttpStatus.OK, message, null, true);
	}

	public Result2 ok(T data) {
		return new Result2(HttpStatus.OK, "", data, true);
	}

	public Result2 ok(String message, T data) {
		return new Result2(HttpStatus.OK, message, data, true);
	}

	public Result2 ok(String message, boolean success) {
		return new Result2(HttpStatus.OK, message, null, success);
	}

	public Result2 clientError(String message) {
		return new Result2(HttpStatus.UNAUTHORIZED, message, null, true);
	}

	public Result2 clientError(String message, T data) {
		return new Result2(HttpStatus.UNAUTHORIZED, message, data, true);
	}

	public Result2 serverError() {
		return new Result2(HttpStatus.INTERNAL_SERVER_ERROR, "服务器异常", null, true);
	}

	public Result2 serverError(String message) {
		return new Result2(HttpStatus.INTERNAL_SERVER_ERROR, message, null, true);
	}

	public Result2 serverError(T data) {
		return new Result2(HttpStatus.INTERNAL_SERVER_ERROR, "", data, true);
	}

	public Result2 serverError(String message, T data) {
		return new Result2(HttpStatus.INTERNAL_SERVER_ERROR, message, data, true);
	}

}