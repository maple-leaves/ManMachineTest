package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;


import javax.persistence.*;

/**
 * @Description: User实体类
 * @Param:
 * @Author:liangjian
 * @Date: 2019.7.10
 */

@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)

@Table(name = "user")

public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	/**
	 *   @Description: Id自增
	 */
	private Long Id;
	@Column(name = "uuid")
	/**
	 *     @Description: 用户UUID，唯一识别
	 */
	private String uuid;
	/**
	 * 用户名
	 */
	@Column(name = "nickname")
	private String nickName;
	/**
	 * @Description: 用户MD5加密后的密码
	 */
	@Column(name = "pwd_md5")
	private String pwdMD5;
	/**
	 * @Description: 用户身法，0为老师，1为学生
	 */
	@Column(name = "identity")
	private boolean identity;
	/**
	 * @Description: 学校
	 */
	@Column(name = "school")
	private String school;
	/**
	 * @Description: 学号
	 */
	@Column(name = "student_id")
	private Long studentId;
	/**
	 * @Description: 成绩
	 */
	@Column(name = "grade")
	private Integer grade;
	/**
	 * @Description: 手机号
	 */
	@Column(name = "phone")
	private String phone;

	@Column(name="is_admin")
	private boolean is_admin;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPwdMD5() {
		return pwdMD5;
	}

	public void setPwdMD5(String pwdMD5) {
		this.pwdMD5 = pwdMD5;
	}

	public boolean isIdentity() {
		return identity;
	}

	public void setIdentity(boolean identity) {
		this.identity = identity;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean getIs_admin(){return is_admin;}

	public void setIs_admin(boolean is_admin){this.is_admin=is_admin;}
}
