package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Description: 考试的实体
 * @Param:
 * @Author:liangjian
 * @Date: 2019.7.10
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "exam")
public class Exam {
	/**
	 * @Description: 试卷ID， 由题库系统生成
	 */
	@Id
	private Long paperId;

	/**
	 * @Description: 年级
	 */
	@Column(name = "grade")
	private Integer grade;

	/**
	 * @Description: 学校
	 */
	@Column(name = "school")
	private String school;

	/**
	 * @Description: 试卷标题
	 */
	@Column(name = "title")
	private String title;

	/**
	 * @Description: 问题数量
	 */
	@Column(name = "problem_num")
	private Integer problemNumber;

	/**
	 * @Description: 试卷总分
	 */
	@Column(name = "score")
	private Integer score;

	/**
	 * @Description:开始时间eg：2019.06.01 09:30:00
	 */
	@Column(name = "start_time")
	private String startTime;

	/**
	 * @Description: 考试时长
	 */
	@Column(name = "duration")
	private String duration;

	/**
	 * @Description: 成绩公布方式
	 */
	@Column(name = "score_publish")
	private String scorePublish;
	/**
	 * @Description: 试卷提交方式
	 */
	@Column(name = "submission_method")
	private String submissionMethod;
	/**
	 * @Description: 考试要求
	 */
	@Column(name = "exam_requirement")
	private String examRequirement;

	public Long getPaperId() {
		return paperId;
	}

	public void setPaperId(Long paperId) {
		this.paperId = paperId;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getProblemNumber() {
		return problemNumber;
	}

	public void setProblemNumber(Integer problemNumber) {
		this.problemNumber = problemNumber;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getScorePublish() {
		return scorePublish;
	}

	public void setScorePublish(String scorePublish) {
		this.scorePublish = scorePublish;
	}

	public String getSubmissionMethod() {
		return submissionMethod;
	}

	public void setSubmissionMethod(String submissionMethod) {
		this.submissionMethod = submissionMethod;
	}

	public String getExamRequirement() {
		return examRequirement;
	}

	public void setExamRequirement(String examRequirement) {
		this.examRequirement = examRequirement;
	}
}
