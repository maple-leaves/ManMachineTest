package com.example.demo.entity;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * @Description: 用来接收题库系统返回的问题集合，并将之打包传到前端,还没用到
 * @Param:
 * @Author:liangjian
 * @Date: 2019.7。11
 */
@Data
@Slf4j
@JsonIgnoreProperties(ignoreUnknown = true)
public class Problem {
	private Long problemId;
	private ProblemProperties properties;

	public Problem(Long problemId, Integer score, String caption, Map<String, String> options) {
		this.problemId = problemId;

		this.properties = new ProblemProperties(caption, score, options);
		// log.info(properties.toString());
	}

}
