package com.example.demo.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@ApiModel("用户名密码实体")
@Slf4j
@Data
@Accessors(chain = true)
public class Name_pwdVo {
	@ApiModelProperty("nickname")
	private String nickname;

	@ApiModelProperty("previousPwd")
	private String previousPwd;

	@ApiModelProperty("password")
	private String password;

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPreviousPwd() {
		return previousPwd;
	}

	public void setPreviousPwd(String previousPwd) {
		this.previousPwd = previousPwd;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}