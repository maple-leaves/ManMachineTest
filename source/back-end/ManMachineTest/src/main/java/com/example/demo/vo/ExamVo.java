package com.example.demo.vo;

import com.example.demo.entity.ExamInformation;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExamVo {
	private List<ExamInformation> examInformationList;
	private Integer ExamNum;

	public ExamVo(List<ExamInformation> examInformationList, Integer ExamNum) {
		this.examInformationList = examInformationList;
		this.ExamNum = ExamNum;
	}

	public List<ExamInformation> getExamInformationList() {
		return examInformationList;
	}

	public void setExamInformationList(List<ExamInformation> examInformationList) {
		this.examInformationList = examInformationList;
	}

	public Integer getExamNum() {
		return ExamNum;
	}

	public void setExamNum(Integer examNum) {
		ExamNum = examNum;
	}
}
