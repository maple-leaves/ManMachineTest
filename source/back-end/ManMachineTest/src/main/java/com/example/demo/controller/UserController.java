package com.example.demo.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.dao.AnswersRepository;
import com.example.demo.dao.ExamRepository;
import com.example.demo.dao.UserExamRepository;
import com.example.demo.dao.UserRepository;
import com.example.demo.dto.ForFalseThread;
import com.example.demo.dto.Result;
import com.example.demo.entity.*;
import com.example.demo.vo.ExamVo;
import com.example.demo.vo.ProblemVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import com.example.demo.dto.Result2;
import com.example.demo.dto.UserDto;
import com.example.demo.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;

import java.io.*;
import java.util.*;


@Api("用户接口")
@RestController
@RequestMapping("/api/user")
@Slf4j
public class UserController {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	UserExamRepository userExamRepository;
	@Autowired
	private ExamRepository examRepository;

	@Autowired
	private AnswersRepository answersRepository;
	@Autowired
	private UserService userService;
	
	@Value("${url}")
 	private  String urlTemp;

	List<Problem> problemsList = new ArrayList<Problem>();
	//    List<ProblemVo> problemVosList=new ArrayList<ProblemVo>();
	String poolId = "4468c74d-759e-4d78-8c43-e1c5405f193b";
	String token = "06e599f3-78db-4c71-b4fa-2b496beab1f6";

	/**
	 * @Author:吕雨润
	 * @Date: 2019.07
	 */

	@ApiOperation(value = "登录")
	@PostMapping("/login")
	public ResponseEntity<Result> Login(@RequestBody String jsonString) {
		int result = userService.login(jsonString);
		if (result == 0) {
			return new ResponseEntity(new Result2().clientError("user does not exits"), HttpStatus.UNAUTHORIZED);
		}
		if (result == 1) {
			return new ResponseEntity(new Result2().ok(), HttpStatus.OK);
		} else {
			return new ResponseEntity(new Result2().clientError("wrong password"), HttpStatus.UNAUTHORIZED);
		}
	}

	@ApiOperation(value = "查看个人信息")
	@PostMapping("/showUserInfo")
	public ResponseEntity<Result> CheckPersonalInformation(@RequestBody String jsonString) {
		UserDto userDto = userService.showUserInfo(jsonString);
		if (userDto == null) {
			return new ResponseEntity(new Result2().clientError("user dose not exits"), HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity(new Result2().ok(userDto), HttpStatus.OK);
	}

	@ApiOperation(value = "修改个人信息")
	@PostMapping("/updateUserInfo")
	public ResponseEntity<Result> ModifyPersonalInformation(@RequestBody String jsonString) {
		userService.updateUserInfo(jsonString);
		return new ResponseEntity(new Result2().ok(), HttpStatus.OK);
	}

	@ApiOperation(value = "修改密码")
	@PostMapping("/updatePwd")
	public ResponseEntity<Result2> ModifyPassword(@RequestBody String jsonString) {
		if (userService.updatePwd(jsonString)) {
			return new ResponseEntity(new Result2().ok("修改成功", true), HttpStatus.OK);
		} else {
			return new ResponseEntity(new Result2().ok("密码错误", false), HttpStatus.OK);
		}
	}




    /*-----------------------------------------------------------------------------------------------------------------------------------*/

	/**
	 * @Description: 用于将学生对应的考试列出来
	 * @Author:liangjian * @完成度：100%
	 * @Date: 2019.7.9
	 */

	private RestTemplate restTemplate = new RestTemplate();

	@ApiOperation(value = "列出所有的考试")
	@RequestMapping(value = "/showExam", method = RequestMethod.POST)
	public Result ShowCourse(@RequestBody JSONObject jsonObject) {

		String nickName = jsonObject.getString("nickname");
		User user = userRepository.findByNickName(nickName);
		List<Exam> exams = examRepository.findByGradeAndSchool(user.getGrade(), user.getSchool());
		List<ExamInformation> examInformations = new ArrayList<ExamInformation>();
		for (int i = 0; i < exams.size(); i++) {
			try {
				Exam exam = exams.get(i);
				UserExam userExam = userExamRepository.findByPaperId(exam.getPaperId());
				ExamInformation examInformation = new ExamInformation(exam.getPaperId(), exam.getTitle(), exam.getStartTime(),
						exam.getDuration(), exam.getScore(), exam.getProblemNumber(), exam.getExamRequirement(), exam.getScorePublish(), exam.getSubmissionMethod(),
						userExam.isCompleted(), userExam.getScore());
				examInformations.add(examInformation);
			} catch (Exception e) {
				Exam exam = exams.get(i);

				ExamInformation examInformation = new ExamInformation(exam.getPaperId(), exam.getTitle(), exam.getStartTime(),
						exam.getDuration(), exam.getScore(), exam.getProblemNumber(), exam.getExamRequirement(), exam.getScorePublish(), exam.getSubmissionMethod(),
						false, 0);
				examInformations.add(examInformation);
			}

		}


//        List<ExamInformation> examInformations=examRepository.findExamParm(user.getGrade(),user.getSchool());
		if (examInformations.isEmpty()) return Result.createBySuccessMessage("恭喜你没有考试！");
		Integer examNum = examInformations.size();
		ExamVo examVo = new ExamVo(examInformations, examNum);
		return Result.createBySuccess(examVo);

	}

	/**
	 * @Description: 要先请求题库系统的试卷API，返回调用结果后，根据调用结果传递到前端
	 * @完成度：90%
	 * @Author:liangjian
	 * @Date: 2019.7.17
	 */
	@ApiOperation(value = "进入考试并返回考试题目")
	@RequestMapping(value = "/entranceExam", method = RequestMethod.POST)
	public Object ShowProblem(@RequestBody JSONObject object) {
		problemsList.clear();
		String nickName = object.getString("nickname");
		Long paperId = object.getLongValue("paperId");
		JSONObject jsonObject = new JSONObject(true);
		Exam exam = examRepository.findByPaperId(paperId);
		String url = urlTemp+"/queryProblems";
		String[] str = new String[1];
		str[0] = exam.getTitle();
		JSONObject jsonObject4 = (JSONObject) SetJson(url, "contains", "tags", str, exam.getProblemNumber());
		JSONArray jsonArray = jsonObject4.getJSONArray("results");

		for (int i = 0; i < jsonArray.size(); i++) {
			Map<String, String> answerMap = new LinkedHashMap<String, String>();
			JSONObject temp = jsonArray.getJSONObject(i);
			answerMap.put("A", temp.getString("choice_A"));
			answerMap.put("B", temp.getString("choice_B"));
			answerMap.put("C", temp.getString("choice_C"));
			answerMap.put("D", temp.getString("choice_D"));
			try {
				Problem problem = new Problem(temp.getLong("problemId"), Integer.valueOf(temp.getString("score")), temp.getString("text"), answerMap);
				problemsList.add(problem);
			} catch (Exception e){
				Problem problem = new Problem(temp.getLong("problemId"), 0, temp.getString("text"), answerMap);
				problemsList.add(problem);
			}
//            problemVosList.add(new ProblemVo(problem,temp.getString("answer")));
		}
//      // log.info(problemsList.toString());
//        log.info("Question is :"+problemVosList.get(0).getProblem().getOrder().toString()+", and its answer is: "+problemVosList.get(0).getAnswer());
		return Result.createBySuccess(problemsList);
	}

	/**
	 * 读取json文件转成String格式
	 */
	public static String readJsonFile(String fileName) {
		String jsonStr = "";
		try {
			File jsonFile = new File(fileName);
			FileReader fileReader = new FileReader(jsonFile);

			Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");
			int ch = 0;
			StringBuffer sb = new StringBuffer();
			while ((ch = reader.read()) != -1) {
				sb.append((char) ch);
			}
			fileReader.close();
			reader.close();
			jsonStr = sb.toString();
			return jsonStr;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
//    /**
//     * @Description: 因为没有题库的API接口，只能将Json文件中的题目读取出来 充当测试
//     * @Param:
//     * @Author:liangjian
//     * @Date:
//     */
////    @ApiOperation(value = "进入考试并返回考试题目")
////    @PostMapping(value = "/entranceExam")
////    public JSONObject GetJason(){
////
////        String path = UserController.class.getClassLoader().getResource("examDetail1.json").getPath();
////        String s =readJsonFile(path);
////        JSONObject jsonObject = JSON.parseObject(s);
////        return  jsonObject ;
////    }

    /*-----------------------------------------------------------------------------------------------------------------------------------*/


	/**
	 * @Description: 用户提交考试答案接口
	 * @Author:简金荣
	 */

	@ResponseBody
	@ApiOperation(value = "用户提交考试答案接口")
	@PostMapping("/submitExamAnswer")
	public Result submitExamAnswer(@RequestBody JSONObject obj) {
		return Result.createBySuccess(toSubmitExamAnswer(obj));
	}

	/**
	 * @Description: 用户查看考试详情接口
	 * @Author:简金荣
	 */
	@ResponseBody
	@ApiOperation(value = "用户查看考试详情接口")
	@PostMapping("/showExamDetail")
	public Result showExamDetail(@RequestBody JSONObject obj) {
		return Result.createBySuccess(toShowExamDetail(obj));
	}

	/**
	 * @Description: 用户提交考试答案接口实现
	 * @Author:简金荣
	 */
	public int toSubmitExamAnswer(JSONObject obj) {
		//先定义成绩，设置值为0
		Integer score = 0;

		try {
			//从前端JSON数据中剥离出各个属性
			String nickname = obj.getString("nickname");
			Long paperId = obj.getLong("paperId");
			String answers = obj.getString("answers");
			JSONArray an = obj.getJSONArray("answers");

			//根据nickname找到数据库中的user
			User user = userRepository.findByNickName(nickname);
			//根据paperId找到title
			Exam exam1 = examRepository.findByPaperId(paperId);
			String title = exam1.getTitle();

			//把数据库中的user的成绩暂时设置为0
			// user.setGrade(0);

			//当获得的answer属性非空时统计答题情况
			if (StringUtils.isNotEmpty(answers)) {
				//把前端JSON数据中的answers数组数据进行抽取
				JSONArray answersArray = JSONArray.parseArray(String.valueOf(an));

				//定义接收前端JSON数据中的answers数组数据的答案类数组并初始化

//                for (int i=0;i<answersArray.size();i++){
//                    answers1[i] = new Answers();
//                }
				System.out.println("000");
				//把答案的JSON数组中的每个属性剥离，用answers1接收
				for (int i = 0; i < answersArray.size(); i++) {
					//

					Answers answers1 = new Answers();
					answers1.setNickname(nickname);
					answers1.setPaperId(paperId);
					answers1.setProblemId(JSONObject.parseObject(JSONObject.toJSONString(answersArray.get(i))).getLong("problemId"));
					answers1.setAnswer(JSONObject.parseObject(JSONObject.toJSONString(answersArray.get(i))).getString("answer"));
					System.out.println("111" + answers1.getNickname() + "/" + answers1.getPaperId() + "/" + answers1.getProblemId() + "/" + answers1.getAnswer() + "/");
					answersRepository.save(answers1);
				}

				//访问外部接口，得到远程数据库数据
				String url = urlTemp+"/queryProblems";
				RestTemplate restTemplate = new RestTemplate();
				//这里更改post给远程数据库的JSON数据中的argument2
				JSONObject obj1 = setJSON();
				obj1.getJSONObject("querry").remove("argument2");
				String[] s = new String[1];
				s[0] = title;
				obj1.getJSONObject("querry").put("argument2", s);
				//此时argument2变为了从exam获得的title，即设计标签为试卷标题

				//这里访问远程数据库，使用的JSON为上面修改后的obj1
				JSONObject yuanchen = new JSONObject();
				try {
					ResponseEntity<JSONObject> object = restTemplate.postForEntity(url, obj1, JSONObject.class);
					yuanchen = object.getBody();
				} catch (RestClientException e) {
					System.out.println("题库找不到此试卷的信息。。。");
					e.printStackTrace();
				}

				//把用户的答案与远程数据库题库中的答案进行比对，并统计成绩
				JSONArray resultArray = new JSONArray();
				for (int i = 0; i < answersArray.size(); i++) {

					//剥离远程数据库题库中的results
					resultArray = yuanchen.getJSONArray("results");
					//定义接收试题答案的变量
					String trueAnswer = new String();
					//用于标识problemId是否存在
					Boolean exit = false;

					Long strProblemId = 0L;
					Long problemId = 0L;
					int l=0;
					for (int j = 0; j < resultArray.size(); j++) {
						//剥离results中的problemId
						strProblemId = JSONObject.parseObject(JSONObject.toJSONString(resultArray.get(j))).getLongValue("problemId");
//                        System.out.println(strProblemId);
						problemId = JSONObject.parseObject(JSONObject.toJSONString(answersArray.get(i))).getLongValue("problemId");
//                        System.out.println(problemId);
						//进行problemId比对
						if (problemId.equals(strProblemId)) {
							exit = true;
//                            int a = Integer.valueOf(JSONObject.parseObject(JSONObject.toJSONString(resultArray.get(j))).getString("answer"));
//                            trueAnswer = String.valueOf((char) a);
							trueAnswer = JSONObject.parseObject(JSONObject.toJSONString(resultArray.get(j))).getString("answer");
							System.out.println(strProblemId + "problemId's answer is " + trueAnswer);
							l=j;
							break;
						}
					}
					if (exit == false) {
						System.out.println("problemId" + JSONObject.parseObject(JSONObject.toJSONString(answersArray.get(i))).getInteger("proplemId") + "不存在");
					} else {
						//进行answer比对
						Answers answers2 = answersRepository.findByPaperIdAndProblemIdAndNickname(paperId, strProblemId, nickname);
						if (answers2.getAnswer()!=null&&answers2.getAnswer().equals(trueAnswer)) {
							score += Integer.valueOf(JSONObject.parseObject(JSONObject.toJSONString(resultArray.get(l))).getString("score"));
						} else {
							//错题本
							new ForFalseThread(problemId,user.getUuid()).start();
						}
					}
				}
				//设置用户成绩
				UserExam userExam = new UserExam();
				userExam.setPaperId(paperId);
				userExam.setScore(score);

				userExam.setUuid(userRepository.findByNickName(nickname).getUuid());
				userExam.setCompleted(true);
				userExam.setScore(score);
				userExamRepository.save(userExam);
				System.out.println("用户成绩为：" + score);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return score;
	}


	/**
	 * @Description: 用户查看考试详情接口实现
	 * @Author:简金荣
	 */
	public JSONObject toShowExamDetail(JSONObject obj) {

		//从前端JSON数据中剥离出各个属性
		String nickname = obj.getString("nickname");
		Long paperId = obj.getLong("paperId");
		System.out.println("paperId:" + paperId);

		//根据paperId找到title
		Exam exam1 = examRepository.findByPaperId(paperId);
		String title = exam1.getTitle();

		//访问外部接口，得到远程数据库数据
		String url = urlTemp+"/queryProblems";
		RestTemplate restTemplate = new RestTemplate();
		//这里更改post给远程数据库的JSON数据中的argument2
		JSONObject obj1 = setJSON();
		obj1.getJSONObject("querry").remove("argument2");
		String[] s = new String[1];
		s[0] = title;
		obj1.getJSONObject("querry").put("argument2", s);
		//此时argument2变为了从exam获得的title，即设计标签为试卷标题

		//这里访问远程数据库，使用的JSON为上面修改后的obj1
		JSONObject yuanchen = new JSONObject();
		try {
			ResponseEntity<JSONObject> object = restTemplate.postForEntity(url, obj1, JSONObject.class);
			yuanchen = object.getBody();
		} catch (RestClientException e) {
			System.out.println("题库找不到此试卷的信息。。。");
			e.printStackTrace();
		}

		JSONObject problemsArray = new JSONObject();
		//最终存入problemsArray的singleProblemsArray和doubleProblemsArray
		JSONArray singleProblemsArray = new JSONArray();
		JSONArray doubleProblemsArray = new JSONArray();

		JSONArray resultArray = new JSONArray();
		//取出resultArray数组的singleProblemsArray和singleProblemsArray
		JSONArray singleProblemsArray0 = new JSONArray();
		JSONArray doubleProblemsArray0 = new JSONArray();
		try {
			resultArray = yuanchen.getJSONArray("results");
			for (int i = 0; i < resultArray.size(); i++) {
				//获取标签数组
				List<String> array = resultArray.getJSONObject(i).getJSONArray("tags").toJavaList(String.class);
				for (int j = 0; j < array.size(); j++) {
					//如果标签数组存在“多选”就把这个问题归为doubleProblemsArray0，否则就是单项
					if (array.get(j) == "多选") {
						doubleProblemsArray0.set(i, resultArray.get(i));
					} else {
						singleProblemsArray0.set(i, resultArray.get(i));
					}
				}
			}

			String spro = singleProblemsArray0.toString();
			String dpro = doubleProblemsArray0.toString();
			//当获得的singleProblemsArray属性非空时,添加前端要求返回的JSON数据
			if (StringUtils.isNotEmpty(spro)) {
				//添加前端要求返回的userAnswer等属性
				for (int i = 0; i < singleProblemsArray0.size(); i++) {
					singleProblemsArray.add(new JSONObject());
					//添加problemId
					Long pId = singleProblemsArray0.getJSONObject(i).getLongValue("problemId");
					singleProblemsArray.getJSONObject(i).put("problemId", pId);

					//添加properties的JSONObject
					JSONObject properties = new JSONObject();
					//properties添加score和caption
					properties.put("score", singleProblemsArray0.getJSONObject(i).getString("score"));
					properties.put("caption", singleProblemsArray0.getJSONObject(i).getString("text"));
					//properties添加options
					JSONObject options = new JSONObject();
					options.put("A", singleProblemsArray0.getJSONObject(i).getString("choice_A"));
					options.put("B", singleProblemsArray0.getJSONObject(i).getString("choice_B"));
					options.put("C", singleProblemsArray0.getJSONObject(i).getString("choice_C"));
					options.put("D", singleProblemsArray0.getJSONObject(i).getString("choice_D"));
					properties.put("options", options);
					//properties添加answer
					properties.put("answer", singleProblemsArray0.getJSONObject(i).getString("answer"));
					singleProblemsArray.getJSONObject(i).put("properties", properties);

					//添加userAnswer
					Answers answers = answersRepository.findByPaperIdAndProblemIdAndNickname(paperId, pId, nickname);
					if (answers == null) {
						singleProblemsArray.getJSONObject(i).put("userAnswer", "unFind");
						singleProblemsArray.getJSONObject(i).put("trueAnswer", false);
					} else {
						singleProblemsArray.getJSONObject(i).put("userAnswer", answers.getAnswer());
						if(singleProblemsArray0.getJSONObject(i).getString("answer").equals(answers.getAnswer())){
							singleProblemsArray.getJSONObject(i).put("trueAnswer", true);
						}
						else {
							singleProblemsArray.getJSONObject(i).put("trueAnswer", false);
						}

					}
				}
				System.out.println("The singleProblemsArray is" + singleProblemsArray);
			}
			//处理doubleProblemsArray，和singleProblemsArray类似
			if (StringUtils.isNotEmpty(dpro)) {
				for (int i = 0; i < doubleProblemsArray0.size(); i++) {
					doubleProblemsArray.add(new JSONObject());
					Long pId = doubleProblemsArray0.getJSONObject(i).getLongValue("problemId");
					doubleProblemsArray.getJSONObject(i).put("problemId", pId);
					//
					JSONObject properties = new JSONObject();
					properties.put("score", doubleProblemsArray0.getJSONObject(i).getString("score"));
					properties.put("caption", doubleProblemsArray0.getJSONObject(i).getString("text"));

					JSONObject options = new JSONObject();
					options.put("A", doubleProblemsArray0.getJSONObject(i).getString("choice_A"));
					options.put("B", doubleProblemsArray0.getJSONObject(i).getString("choice_B"));
					options.put("C", doubleProblemsArray0.getJSONObject(i).getString("choice_C"));
					options.put("D", doubleProblemsArray0.getJSONObject(i).getString("choice_D"));

					properties.put("options", options);
					properties.put("answer", doubleProblemsArray0.getJSONObject(i).getString("answer"));
					//
					doubleProblemsArray.getJSONObject(i).put("properties", properties);

					//添加userAnswer
					Answers answers = answersRepository.findByPaperIdAndProblemIdAndNickname(paperId, pId, nickname);
					if (answers == null) {
						doubleProblemsArray.getJSONObject(i).put("userAnswer", "unFind");
						doubleProblemsArray.getJSONObject(i).put("trueAnswer", false);
					} else {
						doubleProblemsArray.getJSONObject(i).put("userAnswer", answers.getAnswer());
						if(doubleProblemsArray.getJSONObject(i).getString("answer").equals(answers.getAnswer())){
							doubleProblemsArray.getJSONObject(i).put("trueAnswer", true);
						}
						else {
							doubleProblemsArray.getJSONObject(i).put("trueAnswer", false);
						}

					}
				}
				System.out.println("8doubleProblemsArray//" + doubleProblemsArray);
			}

			problemsArray.put("doubleProblemsArray", doubleProblemsArray);
			problemsArray.put("singleProblemsArray", singleProblemsArray);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return problemsArray;
	}


    /*    ----------------------------------------------------------------*/

	@PostMapping(value = "/setJson")
	//设置访问远程数据库的JSON格式
	public JSONObject setJSON() {
		JSONObject obj = new JSONObject(true);
		//修改obj的参数，使得能够从远程题库获得想要的信息
		obj.put("poolId", "4468c74d-759e-4d78-8c43-e1c5405f193b");
		obj.put("token", "06e599f3-78db-4c71-b4fa-2b496beab1f6");

		JSONObject object1 = new JSONObject();
		object1.put("operator", "contains");
		object1.put("argument1", "tags");
		String[] s = new String[1];
		s[0] = "二年级";
		object1.put("argument2", s);
		obj.put("querry", object1);

		obj.put("random", "false");
		obj.put("deep", "false");

		JSONObject object2 = new JSONObject();
		object2.put("sorttype", "desc");
		object2.put("field", "create_time");
		obj.put("ordering", object2);

		JSONObject object3 = new JSONObject();
		object3.put("page", 0);
		object3.put("size", 20);
		object3.put("total", 0);
		obj.put("pagination", object3);
		System.out.println("===>" + obj.toString());
		return obj;
	}

	/**
	 * @Description:用于构建调用题库的Json,并返回其响应内容
	 * @Author:liangjian
	 * @Date:
	 */
	public Object SetJson(String URL, String operator, String argument1, Object argument2, Integer pagesize) {

		JSONObject jsonObject = new JSONObject(true);
		jsonObject.put("poolId", poolId);
		jsonObject.put("token", token);

		//构建请求题库查找题目API的Json格式
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("operator", operator);
		map.put("argument1", argument1);

		map.put("argument2", argument2);
		jsonObject.put("querry", map);
		jsonObject.put("random", false);
		jsonObject.put("deep", false);
		Map<String, Object> map1 = new LinkedHashMap<>();
		map1.put("sorttype", "desc");
		map1.put("field", "create_time");
		jsonObject.put("ordering", map1);
		Map<String, Object> map2 = new LinkedHashMap<>();
		map2.put("page", 0);
		map2.put("size", pagesize - 1);
		map2.put("total", 0);
		jsonObject.put("pagination", map2);
//
//        //返回数据保存在responseEntity中
		ResponseEntity<JSONObject> responseEntity = restTemplate.postForEntity(URL, jsonObject, JSONObject.class);
//
//        List<Problem> problem =new ArrayList<Problem>();
		JSONObject jsonObject3 = responseEntity.getBody();
		return jsonObject3;
	}
}
