package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProblemProperties {
	private Integer score;
	private String caption;
	private Map<String, String> options;

	public ProblemProperties(String caption, Integer score, Map<String, String> options) {

		this.caption = caption;
		this.score = score;
		this.options = options;
	}
//    @Override
//    public String toString(){
//        return "cation："+caption+", options: "+options.toString();
//    }
}
