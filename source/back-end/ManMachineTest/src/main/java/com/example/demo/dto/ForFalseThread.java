package com.example.demo.dto;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;

public class ForFalseThread extends Thread{
	Long problemId;
	String uuid;
	@Value("${url}")
 	private  String urlTemp;
	public ForFalseThread(Long problemId, String uuid) {
		this.problemId = problemId;
		this.uuid = uuid;
	}
	@Override
	public void run(){
		JSONObject jsonObjectForFalse = new JSONObject();
		//访问外部接口，得到远程数据库数据
		String urlForFalse = urlTemp+"/problemStatus";
		RestTemplate restTemplateF = new RestTemplate();
		//这里更改post给远程数据库的JSON数据中的
		jsonObjectForFalse.put("poolId","4468c74d-759e-4d78-8c43-e1c5405f193b");
		jsonObjectForFalse.put("token","06e599f3-78db-4c71-b4fa-2b496beab1f6");
		jsonObjectForFalse.put("problemId",problemId);
		jsonObjectForFalse.put("unionid",uuid);
		jsonObjectForFalse.put("status","错题");
		jsonObjectForFalse.put("date",System.currentTimeMillis());
		//new Date().getTime()
		//这里访问远程数据库，使用的JSON为上面修改后的
		try {
			RestTemplate restTemplate = new RestTemplate();
			// header填充
			LinkedMultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			headers.put("Content-Type", Collections.singletonList("application/json;charset=UTF-8"));
			headers.put("Accept",Collections.singletonList("application/json;charset=UTF-8"));
			// body填充
			HttpEntity<String> request = new HttpEntity<String>(jsonObjectForFalse.toString(), headers);
			restTemplate.put(urlForFalse,request);
			System.out.println(problemId+"put成功。。。");
		} catch (RestClientException e) {
			System.out.println(problemId+"put出错。。。");
			e.printStackTrace();
		}
	}

}