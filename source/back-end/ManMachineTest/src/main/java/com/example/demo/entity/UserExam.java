package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "user_exam")
/**
 * @Description: 用户与考试的中间表
 * @Param:
 * @Author:liangjian
 * @Date:
 */
public class UserExam {
	/**
	 * @Description: ID，无特殊意义，自增
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer Id;

	/**
	 * @Description: 试卷ID
	 */
	@Column(name = "paper_id")
	private Long paperId;

	/**
	 * @Description: UUID, 用户识别
	 */
	@Column(name = "uuid")
	private String uuid;

	/**
	 * @Description: 是否完成试卷，0是未完成，1是完成
	 */
	@Column(name = "completed")
	private boolean isCompleted;

	/**
	 * @Description: 用户未完成考试，成绩默认为0分；若完成，则存储当前用户的成绩
	 */
	@Column(name = "score")
	private Integer score;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public Long getPaperId() {
		return paperId;
	}

	public void setPaperId(Long paperId) {
		this.paperId = paperId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setCompleted(boolean completed) {
		isCompleted = completed;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}
}
