/*
 Navicat Premium Data Transfer

 Source Server         : admin
 Source Server Type    : MySQL
 Source Server Version : 50716
 Source Host           : localhost:3306
 Source Schema         : mmes

 Target Server Type    : MySQL
 Target Server Version : 50716
 File Encoding         : 65001

 Date: 23/07/2019 16:49:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for answers
-- ----------------------------
DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `problem_id` bigint(20) NULL DEFAULT NULL,
  `answer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户答案',
  `nickname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `paper_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 294 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for exam
-- ----------------------------
DROP TABLE IF EXISTS `exam`;
CREATE TABLE `exam`  (
  `paper_id` bigint(20) NOT NULL COMMENT '试卷id',
  `grade` int(2) NULL DEFAULT NULL COMMENT '1~12，表示小学6年，初中三年，高中6年',
  `school` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '试卷属于哪个学校的',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '试卷标题',
  `problem_num` int(3) NULL DEFAULT NULL COMMENT '试卷题目数量',
  `score` int(3) NULL DEFAULT NULL COMMENT '试卷总分',
  `start_time` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '考试开始时间 eg：2019.06.01 09:30',
  `duration` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '考试持续时间 eg：2小时',
  `score_publish` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '成绩公布方式 eg：提交试卷后公布',
  `submission_method` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '考试提交方式 eg：整卷提交',
  `exam_requirement` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '考试要求 eg：开卷考试，满分100分。2题单选，2题多选。考试限时30分钟。',
  PRIMARY KEY (`paper_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自动递增，看用户数量的',
  `uuid` varchar(28) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信返回的UnionID应该是28位，用于多个小程序之间通用的用户认证',
  `nickname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `pwd_md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户密码MD5加密之后的字符串',
  `identity` tinyint(1) NULL DEFAULT NULL COMMENT '0：老师；1：学生',
  `school` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学生所属学校',
  `student_id` bigint(12) NULL DEFAULT NULL COMMENT '学号',
  `grade` int(2) NULL DEFAULT NULL COMMENT '1~12，表示小学6年，初中三年，高中6年',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户手机号',
  `is_admin` tinyint(1) NULL DEFAULT NULL COMMENT '是否为管理员',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uuid`(`uuid`) USING BTREE,
  UNIQUE INDEX `nickname`(`nickname`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'oXcfz03gFeKWWVyCoOjG942uZtxc', 'maple', 'e10adc3949ba59abbe56e057f20f883e', 1, '华农附小', 201625010511, 3, '12345678901', 1);

-- ----------------------------
-- Table structure for user_exam
-- ----------------------------
DROP TABLE IF EXISTS `user_exam`;
CREATE TABLE `user_exam`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '存储用户与对应考试的id，无特殊用处，自动递增',
  `paper_id` bigint(20) NULL DEFAULT NULL COMMENT '试卷id',
  `uuid` varchar(28) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信返回的UnionID应该是28位，用于多个小程序之间通用的用户认证',
  `completed` tinyint(1) NULL DEFAULT NULL COMMENT '用户是否完成该考试  0：未完成；1：已完成',
  `score` int(3) NULL DEFAULT NULL COMMENT '用户未完成考试，成绩默认为0分；若完成，则存储当前用户的成绩',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_exam_ibfk_1`(`paper_id`) USING BTREE,
  INDEX `user_exam_ibfk_2`(`uuid`) USING BTREE,
  CONSTRAINT `user_exam_ibfk_1` FOREIGN KEY (`paper_id`) REFERENCES `exam` (`paper_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_exam_ibfk_2` FOREIGN KEY (`uuid`) REFERENCES `user` (`uuid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
